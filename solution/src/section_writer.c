/// @file
/// @brief writes section to file, should be used in chain with section extractor
#include "section_writer.h"
#include "util.h"
#include <stdio.h>

int write_section_to_file(FILE* in, FILE* out, const struct SectionHeader* sectionHeader){
    if (fseek(in, sectionHeader->pointerToRawData, SEEK_SET) != 0){
        print_err("can't write section");
        return 0;
    }
    char* data[sectionHeader->SizeOfRawData];
    if (!fread(data, sectionHeader->SizeOfRawData, 1, in)) {
        print_err("can't read data in section");
        return 0;
    }
    if (!fwrite(data, sectionHeader->SizeOfRawData, 1, out)) {
        print_err("can't write data of section");
        return 0;
    }
    return 1;
}
