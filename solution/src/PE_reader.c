/// @file
/// @brief module that reads file and gets sections
#include "PE_reader.h"
#include "util.h"
#include <stdio.h>
#include <stdlib.h>


int read_PE_data(FILE* const in, struct PEFile* PE_file){
    if (fseek(in, MAIN_OFFSET, SEEK_SET) != 0){
        print_err("error while reading header(moving seek)");
        return 0;
    }
    if (!fread(&PE_file->header_offset, sizeof (PE_file->header_offset), 1, in)){
        print_err("error while reading header(reading header offset)");
        return 0;
    }
    if (fseek(in, PE_file->header_offset, SEEK_SET) != 0){
        print_err("error while reading header(moving seek)");
        return 0;
    }
    if (!fread(&PE_file->header, sizeof(struct PEHeader), 1, in)) {
        print_err("error while reading header(reading header)");
        return 0;
    }
    if (PE_file->header.magic != PE_MAGIC_NUMBER){
        print_err("bad magic");
        return 0;
    }
    if (fseek(in, PE_file->header.sizeOfOptionalHeader, SEEK_CUR) != 0){
        print_err("error while seeking");
        return 0;
    }

    PE_file->section_headers = malloc(PE_file->header.numberOfSections * sizeof(struct SectionHeader));

    for (uint16_t i = 0; i < PE_file->header.numberOfSections; i++) {
        if (!fread(&PE_file->section_headers[i], sizeof(struct SectionHeader), 1, in)) {
            print_err("error while reading sections names");
            return 0;
        }
    }
    return 1;
}
