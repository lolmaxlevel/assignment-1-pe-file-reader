/// @file
/// @brief Header file for PE_section_extractor.c
#include <stdio.h>

#ifndef ASSIGNMENT_1_PE_FILE_READER_PE_SECTION_EXTRACTOR_H
#define ASSIGNMENT_1_PE_FILE_READER_PE_SECTION_EXTRACTOR_H
/// @brief Main and only function, that performs extraction
/// @param in file descriptor from where we will read section
/// @param out file descriptor to where we will write
/// @param section_name name of section that we want to extract
/// @return 1 if extraction successful else 0
int extract_section_from_PE_file(FILE* in, FILE* out, const char* section_name);

#endif //ASSIGNMENT_1_PE_FILE_READER_PE_SECTION_EXTRACTOR_H
