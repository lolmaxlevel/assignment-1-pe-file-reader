/// @file
/// @brief module that extracts and writes section by it's name from file
#include "PE_reader.h"
#include "PE_section_extractor.h"
#include "section_writer.h"
#include <stdlib.h>
#include <string.h>


int extract_section_from_PE_file(FILE* const in, FILE* const out, const char* const section_name) {
    struct PEFile PE_file = {0};

    if (read_PE_data(in, &PE_file)) {
        for (uint16_t i = 0; i < PE_file.header.numberOfSections; i++) {
            if (strcmp(PE_file.section_headers[i].name, section_name) == 0) {
                if(write_section_to_file(in, out, &PE_file.section_headers[i]) == 1){
                    free(PE_file.section_headers);
                    return 1;
                }
                else{
                    free(PE_file.section_headers);
                    return 0;
                }
            }
        }
        free(PE_file.section_headers);
    }
    return 0;
}
