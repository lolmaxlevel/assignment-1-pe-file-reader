/// @file 
/// @brief Main application file

#include "PE_section_extractor.h"
#include "util.h"
#include <stdio.h>
/// Application name string
#define APP_NAME "section-extractor"

/// @brief Print usage test
/// @param[in] f File to print to (e.g., stdout)
void usage(FILE *f)
{
  fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char** argv)
{
    if (argc < 4) {
        usage(stdout);
        return 0;
    }
    FILE *in = fopen(argv[1], "rb");
    FILE *out = fopen(argv[3], "wb");
    if (!in || !out) {
        fclose(in);
        fclose(out);
        print_err("can't open files for reading or writing");
        return -1;
    }
    if(extract_section_from_PE_file(in, out, argv[2]) != 1){
        fclose(in);
        fclose(out);
        return -1;
    }
    fclose(in);
    fclose(out);
  return 0;
}
