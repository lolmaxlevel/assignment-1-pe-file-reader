/// @file
/// @brief Module that works with a PE file
#ifndef PE_HEADER
#define PE_HEADER

#include <stdint.h>
#include <stdio.h>

/// Offset for PE file
#define MAIN_OFFSET 0x3c

/// Magic number that defines PE file
#define PE_MAGIC_NUMBER 0x00004550

#ifdef _MSK_VER
#pragma pack(push, 1)
#endif

/// @brief Structure contains COFF File Header
struct
#ifdef __GNUC__
        __attribute__((packed))
#endif
PEHeader {
    /// Magic offset
    uint32_t magic;
    /// The number that identifies the type of target machine.
    uint16_t machine;
    /// The number of sections. This indicates the size of the section table, which immediately follows the headers.
    uint16_t numberOfSections;
    /// The low 32 bits of the number of seconds since 00:00 January 1, 1970 (a C run-time time_t value), which indicates when the file was created.
    uint32_t timeDateStamp;
    /// The file offset of the COFF symbol table, or zero if no COFF symbol table is present. This value should be zero for an image because COFF debugging information is deprecated.
    uint32_t pointerToSymbolTable;
    /// The number of entries in the symbol table. This data can be used to locate the string table, which immediately follows the symbol table. This value should be zero for an image because COFF debugging information is deprecated.
    uint32_t numberOfSymbols;
    /// The size of the optional header, which is required for executable files but not for object files. This value should be zero for an object file.
    uint16_t sizeOfOptionalHeader;
    /// The flags that indicate the attributes of the file.
    uint16_t characteristics;
};

/// @brief Structure contains Section Header
struct
#if defined __GNUC__
        __attribute__((packed))
#endif
SectionHeader
{
    /// An 8-byte, null-padded UTF-8 encoded string.
    char name[8];
    /// The total size of the section when loaded into memory
    uint32_t virtualSize;
    /// For executable images, the address of the first byte of the section relative to the image base when the section is loaded into memory.
    uint32_t virtualAddress;
    /// The size of the section (for object files) or the size of the initialized data on disk (for image files)
    uint32_t SizeOfRawData;
    /// The file pointer to the first page of the section within the COFF file.
    uint32_t pointerToRawData;
    /// The file pointer to the beginning of relocation entries for the section. This is set to zero for executable images or if there are no relocations.
    uint32_t pointerToRelocations;
    ///The file pointer to the beginning of line-number entries for the section. This is set to zero if there are no COFF line numbers.
    uint32_t pointerToLineNumbers;
    /// The number of relocation entries for the section. This is set to zero for executable images.
    uint16_t numberOfRelocations;
    /// The number of line-number entries for the section. This value should be zero for an image because COFF debugging information is deprecated.
    uint16_t numberOfLineNumbers;
    /// The flags that describe the characteristics of the section.
    uint32_t characteristics;
};


/// Structure containing PE file data
struct
#ifdef __GNUC__
        __attribute__((packed))
#endif
        PEFile
{
    /// @name Offsets within file
    ///@{

    /// Offset to a file magic
    uint32_t magic_offset;
    /// Offset to a main PE header
    uint32_t header_offset;
    /// Offset to a section table
    uint32_t section_header_offset;
    ///@}

    /// @name File headers
    ///@{

    /// File magic
    uint32_t magic;
    /// Main header
    struct PEHeader header;
    /// Array of section headers with the size of header.number_of_sections
    struct SectionHeader *section_headers;
    ///@}
};


#ifdef _MSK_VER
#pragma pack(pop)
#endif

/// @brief Read PE file headers into the structure
/// @param in Input file
/// @param PE_file Structure describing file headers
/// @return 1 in case of success else 0
int read_PE_data (FILE* in, struct PEFile* PE_file);

#endif
