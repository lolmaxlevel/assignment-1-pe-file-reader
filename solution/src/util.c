/// @file
/// @brief module with utility functions, should be used only in this lib
#include "util.h"
#include <stdio.h>
/// @brief prints message to stderr
/// @param msg message to print
void print_err(char *msg) { fprintf(stderr, "%s", msg); }
