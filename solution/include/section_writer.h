/// @file
/// @brief Header file for section_writer.c
#ifndef ASSIGNMENT_1_PE_FILE_READER_SECTION_WRITER_H
#define ASSIGNMENT_1_PE_FILE_READER_SECTION_WRITER_H
#include "PE_reader.h"
/// @brief main function that writes section to file
/// @param in file descriptor from where we will read section data
/// @param out file descriptor to where we will write
/// @param sectionHeader header of section that we want to write should be taken from section_extractor
/// @return 1 if writing successful else 0
int write_section_to_file(FILE* in, FILE* out, const struct SectionHeader* sectionHeader);

#endif //ASSIGNMENT_1_PE_FILE_READER_SECTION_WRITER_H
